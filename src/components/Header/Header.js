import React from 'react'
import './Header.css';
import logo from '../../logo.svg';
import {Link, BrowserRouter as Router} from 'react-router-dom';
let constants  = require('../../constant');
function Headers(){
        return(
            <Router>
            <header className="MainHeader">
                <div className="HeaderBar">
                    <img className="Logo" src={logo} width="100" alt="logo"/>
                    <div className="items">
        <div className="itemsList"><Link to="/">{constants.home}</Link> </div>
        <div className="itemsList"><Link to="/team">{constants.team}</Link> </div>
        <div className="itemsList"><Link to="/">{constants.about}</Link> </div>
        <div className="itemsList"><Link to="/">{constants.contact}</Link> </div>
                    </div>
                    <div className="Search">
                        <input className="form-input" placeholder="Search"/>
                    </div>
                </div>
            </header>
            
            </Router>
        )
}

export default Headers

