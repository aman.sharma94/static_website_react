import React from 'react';
import './Home.css';
import FirstSection from './Childcomponent/FirstSection';
import  Slider from './Childcomponent/Slider';
import Project from './Childcomponent/Project';

function Home(){
    return (
        <div className="Home">
            <FirstSection/>
            <Slider/>
            <Project/>
        </div>
    );
}


export default Home;