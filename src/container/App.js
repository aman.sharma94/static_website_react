import { Component } from 'react';
import './App.css';
import Header from '../components/Header/Header';
// import Footer from '../components/Footer/Footer';
import Home from '../components/Home/Home';
import Team from '../components/Team/Team';
import { BrowserRouter as Router,Switch,Route } from 'react-router-dom';
class App extends Component{
  render(){
    return(
<div className="App">
    <Header/>
      
      <Router>
        <Switch>
          <Route exact path="/">
            <Home/>
          </Route>
          <Route  path="/team">
          <Team/>
          </Route>
        </Switch>
      </Router>
    </div>
    )
  }
}

export default App;
